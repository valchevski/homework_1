<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:d="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="d:Guests">
        <xsl:element name="Guests">
            <xsl:for-each select="d:Guest|end:Guest">
                <xsl:value-of
                    select="concat(@Age, '/', @Nationalty, '/', @Gender, '/', @Name, '/', ./d:Type/text(), '/', ./d:Profile/d:Address/text(), '|')"/>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
